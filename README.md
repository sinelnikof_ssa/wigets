wigets for views
================
Виджеты для упращения шаблонизации

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist sinelnikof/yii2-wigets "*"
```

or addasdasdasd

```
"sinelnikof/yii2-wigets": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :
test2
```php

'controllerMap' => [
       'wigetBox'=>sinelnikof\wigets\controllers\WigetController::class,
...
]

<?= \sinelnikof\wigets\AutoloadExample::widget(); ?>

<?=

\sinelnikof\wigets\Box::widget([
    'id'            => 'yt0',
    'solid'         => false,
    'can_deleted'   => true,
    'can_collapsed' => true,
    'info'          => true,
    'header'        => false, // отключает хедер
    'subMenu'       => [['label' => 'About', 'url' => ['site/about']]],
    'type'          => \sinelnikof\wigets\Box::BOX_TYPE_DANGER,
    'name'          => 'SimpleHeader',
    'text'          => 'SimpleText',
    'btns'          => ['текс','ссылка','кнопка'],
    'footer'        => 'SimpleFooter',
]);
?>
<?=
\sinelnikof\wigets\Box::widget([
    'id'            => 'yt4',
    'solid'         => false,
    'can_deleted'   => true,
    'can_collapsed' => true,
    'info'          => true,
    'subMenu'       => [['label' => 'About', 'url' => ['site/about']]],
    'type'          => \sinelnikof\wigets\Box::BOX_TYPE_SUCCESS,
    'name'          => 'SimpleHeader',
    'text'          => 'SimpleText',
    'footer'        => 'SimpleFooter',
]);
?>
<?=
\sinelnikof\wigets\SmallBox::widget([
    'id'       => 'wwe',
    'url'      => ['site/login'],
    'text'     => 'SimpleText',
    'moreText' => 'SimpleMoreText',
    'type'     => \sinelnikof\wigets\SmallBox::TYPE_SUCCESSS,
]);
?>


<?= \sinelnikof\wigets\Callout::widget(); ?>

```
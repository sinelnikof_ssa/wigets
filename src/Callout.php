<?php

/*
 * Gonnyh Ivan
 * sinelnikof88@gmail.com
 * Developing  by Yii
 * Each line should be prefixed with  * 
 */

namespace sinelnikof\wigets;

/**
 * Description of Callout
 *
 * @author Gonnyh.I
 */
class Callout extends \yii\base\Widget {

    const TYPE_INFO = 'callout-info';
    const TYPE_DANGER = 'callout-danger';
    const TYPE_WARNING = 'callout-warning';
    const TYPE_SUCSESS = 'callout-success';

    public $name = '';
    public $text = '';
    public $type = '';

    public function run() {
        return $this->render('callout', [
                    'name' => $this->name,
                    'text' => $this->text,
                    'type' => $this->type,
        ]);
    }

}

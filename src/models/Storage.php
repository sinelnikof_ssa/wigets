<?php
namespace sinelnikof\wigets\models;


class Storage extends \yii\redis\ActiveRecord {

    /**
     * @return array the list of attributes for this record
     */
    public function attributes() {
        return ['id', 'user_id', 'name', 'value'];
    }

}

<?php

namespace sinelnikof\wigets;

/**
 * This is just an example.
 */
class Box extends \yii\base\Widget {

    public static $sessName = 'sessBox3';

    const BOX_TYPE_INFO = ' box-info ';
    const BOX_TYPE_SUCCESS = ' box-success ';
    const BOX_TYPE_DANGER = ' box-danger ';
    const BOX_TYPE_WARNING = ' box-warning  ';

    public $id; // ident
    public $text; // content
    public $name; //label box
    public $can_deleted; //is box can delete
    public $can_collapsed; //is box can collapse
    public $solid = true; // visual defference collorification header box
    public $info; // info message
    public $footer = false;
    public $type;
    public $subMenu;
    public $header = true;
    public $btns = [];
    public $is_open = true;
    public $padding = true;
    public $btn = [];

    public function init() {
        $url = \yii\helpers\Url::toRoute('/wigetBox/change-box-state/');
        $js = <<<JS
function colloapsedBox(_id) {
    setTimeout(function(){
        let clname = ".indic-" + _id;
        let sel = $(clname).hasClass('fa-minus')
        data  = {name:_id,isViewed:sel}
        $.get('$url',data)
    },200)
}    
JS;
        $this->getView()->registerJs($js, \yii\web\View::POS_BEGIN);
    }

    public function run() {
        $this->type = $this->type ?? self::BOX_TYPE_INFO;

        $state = self::getState($this->id);
        if (!$state || $state->value == 'Y') {
            $this->is_open = true;
        } else {
            $this->is_open = false;
        }


        return $this->render('box', ['model' => $this]);
    }

    public static function getState($name) {
        $name = self::$sessName . $name;

        return models\Storage::find()->where(['user_id' => (string) \Yii::$app->user->id, 'name' => $name])->one(); // find by query
    }

    public static function changeState($name) {
        $customer = self::getState($name);
        if (!$customer) {
            $customer = new models\Storage;
            $customer->user_id = (string) \Yii::$app->user->id;
            $customer->name = self::$sessName . $name;
        }
        if ($customer->value == 'N') {
            $customer->value = 'Y';
        } else {
            $customer->value = 'N';
        }
        $customer->save();
    }

}

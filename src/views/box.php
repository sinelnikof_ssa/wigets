<?php /*
  'id' =>  $model->this->id ?? uniqid(),
  'text' =>  $model->this->text,
  'name' =>  $model->this->name,
  'is_deleted' =>  $model->this->is_deleted,
  'is_collapsed' =>  $model->this->is_collapsed,
  'is_solid' =>  $model->this->is_solid,
  'info' =>  $model->this->info,
  'url' =>  $model->this->url,
  'fotter' =>  $model->this->fotter,
  'type' =>  $model->this->type,
 */ ?>

<div id="<?= $model->id ?>" class="box <?= $model->solid ? "box-solid " : '' ?> <?= $model->type ?> <?= !$model->is_open ? "collapsed-box" : '' ?>">
    <?php if ($model->header): ?>
        <div class="box-header with-border">
            <h3 class="box-title"><?= $model->name ?></h3>
            <div class="box-tools pull-right"> 

                <?php if (!empty($model->btns)): ?>
                    <?php foreach ($model->btns as $btn): ?>
                        <?= $btn ?>
                    <?php endforeach; ?>
                <?php endif; ?>

                <?php if ($model->can_collapsed): ?>
                    <button onclick="colloapsedBox('<?= $model->id ?>')" type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus indic-<?= $model->id ?>"></i>
                    </button>
                <?php endif; ?>
                <?php if ($model->subMenu): ?>   
                    <div class = "btn-group">
                        <button type = "button" class = "btn btn-box-tool dropdown-toggle" data-toggle = "dropdown" aria-expanded = "false"><i class = "fa fa-wrench"></i></button>
                        <?=
                        \yii\widgets\Menu::widget([
                            'options' => ['class' => 'dropdown-menu'],
                            'items' => $model->subMenu
//                             ['label' => 'About', 'url' => ['site/about']],
                        ]);
                        ?>
                    </div>
                <?php endif; ?>
                <?php if ($model->info): ?>
                    <span data-toggle="tooltip"  data-placement="left" title="" class="btn btn-box-tool"
                          data-original-title="<?= $model->info ?>"><i class="fas fa-question"></i>    
                    </span>         
                <?php endif; ?>
                <?php if ($model->can_deleted): ?>
                    <button type = "button" class = "btn btn-box-tool" data-widget = "remove"><i class = "fa fa-times"></i></button>
                <?php endif; ?>

            </div>
        </div>
    <?php endif; ?>
    <div class = "box-body <?= $model->padding ? "" : ' no-padding' ?>" style = "">
        <?= $model->text ?>
    </div>
    <!-- /.box-body -->
    <?php if ($model->footer): ?>
        <div class="box-footer" style="">
            <?= $model->footer ?>
        </div>
    <?php endif; ?>
</div>
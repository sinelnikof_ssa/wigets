<div id="<?= $model->id ?>" class="small-box <?= $model->type ?>">
    <div class="inner">
        <p><?= $model->text ?></p>
    </div>
    
    <div class="icon">
        <i class="fa fa-bar-chart"></i>
    </div>
    <?php if ($model->url): ?>
        <a class="small-box-footer"   href="<?= yii\helpers\Url::to($model->url) ?>"           >
            <?= $model->moreText ?> <i class="fa fa-arrow-circle-right"> </i>
        </a>  
    <?php endif; ?>

</div>  
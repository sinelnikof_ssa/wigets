<?php

/*
 * Gonnyh Ivan
 * sinelnikof88@gmail.com
 * Developing  by Yii
 * Each line should be prefixed with  * 
 */

namespace sinelnikof\wigets\controllers;

/**
 * Description of BoxController
 *
 * @author Gonnyh.I
 */
use Yii;
use yii\base\ErrorException;
use yii\db\StaleObjectException;
use yii\web\Controller;
use sinelnikof\wigets\Box;

class WigetController extends Controller {

    // изменеие и сохранение состояния бокса
    public function actionChangeBoxState($name) {
        Box::changeState($name);
        return;
    }

}

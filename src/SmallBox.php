<?php

/*
 * Gonnyh Ivan
 * sinelnikof88@gmail.com
 * Developing  by Yii
 * Each line should be prefixed with  * 
 */

namespace sinelnikof\wigets;

/**
 * Description of Callout
 *
 * @author Gonnyh.I
 */
class SmallBox extends \yii\base\Widget {

    const TYPE_INFO     = 'bg-aqua';
    const TYPE_DANGER   = 'bg-red';
    const TYPE_WARNING  = 'bg-yellow';
    const TYPE_SUCCESSS = 'bg-green';

    public $id;
    public $url;
    public $text     = '';
    public $moreText = '';
    public $type     = '';

    public function run() {
        if (!$this->id) {
            $this->id = $this->getId();
        }
        return $this->render('smallBox', [
                    'model' => $this,
        ]);
    }

}
